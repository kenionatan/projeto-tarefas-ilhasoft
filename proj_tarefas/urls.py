"""proj_tarefas URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
# from tarefas import urls as tarefas_urls
from django.contrib.auth import views as auth_views
from home import urls as home_urls
from rest_framework import routers
from tarefas.api.viewsets import TarefaViewSet
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = routers.DefaultRouter()
router.register(r'tarefasapi', TarefaViewSet)

urlpatterns = [
    # Urls para gerar token da api
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),

    # Url para consumir a api
    path('tarefasapi/', include(router.urls)),

    # Urls da aplicação
    path('', include(home_urls), name="home"),
    path('login/', auth_views.LoginView.as_view(), name="login"),
    path('logout/', auth_views.LogoutView.as_view(), name="logout"),
    path('admin/', admin.site.urls),
    path('tasks/', include('tarefas.urls')),
    #path('tasks/', include(tarefas_urls))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
