from django.forms import ModelForm
from .models import Tarefa

class TaskForm(ModelForm):
    class Meta:
        model = Tarefa
        fields = ['nome_tarefa', 'status_tarefa', 'descricao_tarefa']