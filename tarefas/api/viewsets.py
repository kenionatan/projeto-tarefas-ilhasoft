from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from tarefas.models import Tarefa
from .serializers import TarefaSerializer

class TarefaViewSet(ModelViewSet):

    permission_classes = (IsAuthenticated,)
    queryset = Tarefa.objects.all()
    serializer_class = TarefaSerializer