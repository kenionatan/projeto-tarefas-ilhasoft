from rest_framework.serializers import ModelSerializer
from tarefas.models import Tarefa

class TarefaSerializer(ModelSerializer):
    class Meta:
        model = Tarefa
        fields = ('id', 'nome_tarefa', 'status_tarefa', 'data_criacao_tarefa', 'user')