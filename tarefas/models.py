from django.db import models
from django.utils import timezone
from django.conf import settings

TIPOS_STATUS = (
    (1, "A Fazer"),
    (2, "Concluída"),
)

# Criando classe de Tarefa, definindo atributos da classe no model
class Tarefa(models.Model):
    # views.py se encarrega de setar o usuário logado no momento do cadastro
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    nome_tarefa = models.CharField(max_length=150)
    status_tarefa = models.IntegerField(blank=True, null=True, choices=TIPOS_STATUS)
    descricao_tarefa = models.TextField(blank=True, null=True)
    data_criacao_tarefa = models.DateTimeField(default=timezone.now, blank=True, null=True)
    data_ultima_alteracao = models.DateTimeField(auto_now=True, blank=True, null=True)

    def __str__(self):
        return self.nome_tarefa

