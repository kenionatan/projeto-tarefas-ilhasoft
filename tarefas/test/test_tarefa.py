from django.test import TestCase
from tarefas.models import Tarefa
from tarefas.views import ListaTarefa
from django.urls import reverse, resolve
from rest_framework.test import RequestsClient, APIRequestFactory
from django.contrib.auth.models import User


class TarefaTestCase(TestCase):
    def setUp(self):
        self.tarefa = Tarefa.objects.create(nome_tarefa="Tarefa Teste")

    def test_verifica_num_tarefa(self):
        assert Tarefa.objects.all().count() == 1

    def test_verifica_status_tarefa(self):
        assert self.tarefa.status_tarefa is None

    def test_verifica_url_lista(self):
        url = reverse('task_list')
        self.assertEquals(resolve(url).func.view_class, ListaTarefa)

    def test_verifica_data(self):
        tarefa = Tarefa.objects.create(nome_tarefa="Tarefa Teste")
        self.assertIsNotNone(self.tarefa.data_criacao_tarefa)

    def test_resposta_token(self):
        User.objects.create_user(username='admin', password='admin')
        client = RequestsClient()
        response = client.post('http://127.0.0.1:8000/api/token/', json={
            'username': 'admin',
            'password': 'admin'
        })
        resp = response.json()
        print(resp['access'])
        assert response.status_code == 200

    def test_consumo_api(self):
        User.objects.create_user(username='admin', password='admin')
        client = RequestsClient()

        response = client.post('http://127.0.0.1:8000/api/token/', json={
            'username': 'admin',
            'password': 'admin'
        })
        resp = response.json()
        print(resp['access'])
        token_gerado = resp['access']

        client.headers.update({'Authorization': 'Bearer '+token_gerado})
        response = client.get('http://127.0.0.1:8000/tarefasapi/tarefasapi/')
        print(response.json())
        assert response.status_code == 200
