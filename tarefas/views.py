from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from .models import Tarefa
from .forms import TaskForm
from django.views.generic import(
    ListView,
    CreateView,
    UpdateView,
    DeleteView
)

# Listar Tarefas
class ListaTarefa(ListView):
    template_name = "tarefas.html"
    def get_queryset(self, *args, **kwargs):
        return Tarefa.objects.filter(user=self.request.user)

# Nova Tarefa
class CriaTarefa(CreateView):
    template_name = 'nova_tarefa.html'
    form_class = TaskForm

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(CriaTarefa, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('task_list')

# Update Tarefa
class UpdateTarefa(UpdateView):
    model = Tarefa
    template_name = 'nova_tarefa.html'
    form_class = TaskForm
    pk_url_kwarg = 'id'
    def get_success_url(self):
        return reverse('task_list')

# Deleta Tarefa
class DeleteTarefa(DeleteView):
    model = Tarefa
    template_name = 'tarefa_delete_confirm.html'
    pk_url_kwarg = 'id'
    def get_success_url(self):
        return reverse('task_list')

@login_required
def tasks_delete(request, id):
    tarefa = get_object_or_404(Tarefa, pk=id)

    if request.method == 'POST':
        tarefa.delete()
        return redirect('task_list')
    return render(request, 'tarefa_delete_confirm.html', {'tarefa': tarefa})