from django.urls import path
from .views import (
    ListaTarefa,
    CriaTarefa,
    UpdateTarefa,
    DeleteTarefa
)
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('lista/', login_required(ListaTarefa.as_view()), name="task_list"),
    path('new/', CriaTarefa.as_view(), name="task_new"),
    path('update/<int:id>/', login_required(UpdateTarefa.as_view()), name="task_update"),
    path('delete/<int:id>/', login_required(DeleteTarefa.as_view()), name="task_delete")
]